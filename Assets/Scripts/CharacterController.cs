using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    private Vector3 velocity;
    public float jumpForce = 500;
    public float input;
    public int speed = 3;
    public float maxVelocity;
    public Rigidbody2D rb;
    public bool jump;
    private bool isGrounded = true;
    public LayerMask groundLayerMask;
    public float radius;
    public float distance ;
    private bool isPlaying = true;
    
    private void FixedUpdate()
    {
        if (!isPlaying)
            return;
     
        velocity.x = speed * Time.fixedDeltaTime * input;
        velocity.y = rb.velocity.y;
        rb.velocity= velocity;
    }
    private void Update()
    {
        if (!isPlaying)
            return;

        if (Physics2D.CircleCast(transform.position, radius, - Vector3.up, distance, groundLayerMask))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

        if (jump && isGrounded)
        {
            rb.velocity = Vector3.right * rb.velocity.x;
            rb.AddForce(Vector3.up * jumpForce);
            jump = false;
            isGrounded = false;
        }

        if (transform.position.y < -30)
        {
            isPlaying = false;
            Game.I.OnPlayerDied();
        }
        if(transform.position.x > 98.49)
        {
            
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position - (Vector3.up)*distance, radius);

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("spike"))
        {
            isPlaying = false;
            Game.I.OnPlayerDied();
        }else if (collision.gameObject.CompareTag("win"))
        {
            isPlaying = false;
            Game.I.OnPlayerWin();
        } 
    }

}
