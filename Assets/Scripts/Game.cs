using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game I;
    public GameObject defeat;
    public GameObject canvas;
    public GameObject victory;

    private void Awake()
    {
        I = this;
    }
    private void Start()
    {
        defeat.SetActive(false);
        victory.SetActive(false);
    }

    public void OnPlayerDied()
    {
        Time.timeScale = 0;
        defeat.SetActive(true);
    }
    public void OnPlayerWin()
    {
        Time.timeScale = 0;
        victory.SetActive(true);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
