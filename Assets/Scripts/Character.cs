using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public CharacterController controller;
    
    private void Update()
    {
        controller.input = Input.GetAxisRaw("Horizontal");
        controller.jump = Input.GetButtonDown("Jump");

}
}
